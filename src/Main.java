import javax.imageio.IIOException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        Test test = new Test();
        File inputFile = new File("note.txt");
        File outputFile = new File("output.txt");
        File testFile = new File("test.txt");
        File solvedFile = new File("solved.txt");

        try{
            test.testNote(inputFile,outputFile,testFile);

            FileReader outputReader = new FileReader(outputFile);
            BufferedReader outputBufferedReader = new BufferedReader(outputReader);

            FileReader solvedReader = new FileReader(solvedFile);
            BufferedReader solvedBufferedReader = new BufferedReader(solvedReader);

            String outputLine = outputBufferedReader.readLine();
            String solvedLine = solvedBufferedReader.readLine();

            int count = 0;

            while (outputLine!=null && solvedLine!=null){
                count++;
                if (outputLine.equals(solvedLine)) {
                    System.out.println("Testul "+count+": " + "passed");
                } else {
                    System.out.println("Testul "+count+": " + "failed");
                }
                outputLine = outputBufferedReader.readLine();
                solvedLine = solvedBufferedReader.readLine();
            }

            outputBufferedReader.close();
            solvedBufferedReader.close();

            if(outputLine!=null && solvedLine==null){
                System.out.println("Nu au fost realizate destule teste pentru datele inserate!");
            } else if (outputLine==null && solvedLine!=null) {
                System.out.println("Nu au rezultat destule date pentru toate testele!");
            }

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}