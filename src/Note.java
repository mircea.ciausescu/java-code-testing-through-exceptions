public class Note {
    public int[] arrayNote;

    public int[] getarrayNote() {
        return arrayNote;
    }

    public void setArrayNote(String[] arrayNote) {
        if (arrayNote!=null){
            this.arrayNote = new int[arrayNote.length];
            for (int i = 0;i<arrayNote.length;i++){
                this.arrayNote[i] = Integer.parseInt(arrayNote[i]); // aici imi da eroare daca sirul este gol
            }
        } else {
            this.arrayNote=null;
        }
    }

    public void sort(int n) {
        int temp;
        boolean swapped;
        for (int i = 0; i < n - 1; i++) {
            swapped = false;
            for (int j = 0; j < n - 1 - i; j++) {
                if (arrayNote[j] < arrayNote[j + 1]) {
                    temp = arrayNote[j];
                    arrayNote[j] = arrayNote[j + 1];
                    arrayNote[j + 1] = temp;
                    swapped = true;
                }
            }
            if (!swapped) {
                break;
            }
        }
    }

    public int getVal(int poz, int n) throws CustomException {
        if (arrayNote == null){
            throw new CustomException("Vectorul de note nu poate fi gol!");
        }
        if (arrayNote.length>n){
            throw new CustomException("Sirul este mai mare decat lungimea data!");
        }
        if (poz <= 0) {
            throw new CustomException("Pozitia nu poate fi mai mica sau egala cu 0!");
        }
        if (n == 0) {
            throw new CustomException("Sirul nu poate fi gol!");
        }
        sort(n);
        if (arrayNote[n - 1] <= 0) {
            throw new CustomException("Nu se pot acorda note mai mici ca 1!");
        }
        int[] unique = new int[n];
        int m = 0;
        for (int i = 0; i < n - 1; i++) {
            if (arrayNote[i] != arrayNote[i + 1]) {
                unique[m++] = arrayNote[i];
            }
        }
        unique[m++] = arrayNote[n - 1];
        if (poz > m) {
            throw new CustomException("Nu exista aceasta pozitie in sir!");
        }
        return unique[poz - 1];
    }

    public int backup(int poz) throws CustomException {
        if (arrayNote == null){
            throw new CustomException("Vectorul de note nu poate fi gol!");
        }
        if (poz <= 0) {
            throw new CustomException("Pozitia nu poate fi mai mica sau egala cu 0!");
        } else {
            int n = arrayNote.length;
            if (n == 0) {
                throw new CustomException("Sirul nu poate fi gol!");
            } else {
                sort(n);
                if (arrayNote[n - 1] <= 0) {
                    throw new CustomException("Nu se pot acorda note mai mici ca 1");
                } else {
                    int[] unique = new int[n];
                    int m = 0;
                    for (int i = 0; i < n - 1; i++) {
                        if (arrayNote[i] != arrayNote[i + 1]) {
                            unique[m++] = arrayNote[i];
                        }
                    }
                    unique[m++] = arrayNote[n - 1];
                    if (poz > m) {
                        throw new CustomException("Nu exista aceasta pozitie in sir!");
                    } else {
                        return unique[poz - 1];
                    }
                }
            }
        }
    }
}