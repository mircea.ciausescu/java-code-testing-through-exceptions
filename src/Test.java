import java.io.*;

public class Test {
    public void testNote(File inputFile, File outputFile, File testFile) throws IOException {
        if (inputFile==null){
            throw new IllegalArgumentException("File argument cannot be null");
        }

        FileReader fileReader = new FileReader(inputFile);
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        FileReader testReader = new FileReader(testFile);
        BufferedReader testBufferedReader = new BufferedReader(testReader);

        PrintWriter printWriter = new PrintWriter(outputFile);

        Note note = new Note();

        String inputLine;
        inputLine = bufferedReader.readLine();

        String testLine;
        testLine = testBufferedReader.readLine();

        int count=0;

        while(inputLine!=null && testLine!=null){
            int n = Integer.parseInt(inputLine);
            int poz = Integer.parseInt(testLine);

            count++;

            inputLine = bufferedReader.readLine();

            String[] stringNumbers;

            if(inputLine==null){
                stringNumbers=null;
            } else {
                stringNumbers = inputLine.split(" ");
            }
            note.setArrayNote(stringNumbers);

            try{
                int rezultat = note.getVal(poz,n);
                printWriter.println("Testul "+count+": " + rezultat);
            } catch (CustomException e) {
                printWriter.println("Testul "+count+": " + e.getMessage());
            }

            inputLine = bufferedReader.readLine();
            testLine = testBufferedReader.readLine();
        }

        bufferedReader.close();
        printWriter.close();

        if (inputLine!=null && testLine==null){
            throw new IOException("Nu au fost realizate destule teste pentru toate datele!");
        } else if (inputLine==null && testLine!=null) {
            throw new IOException("Nu au fost oferite destule date pentru toate testele!");
        }
    }
}
